﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPExample.Accessories
{
    public class MilkPod
    {
        public double Capacity { get; set; }

        public MilkPod(double capacity)
        {
            this.Capacity = capacity;
        }

        public void Heat()
        {
            Console.WriteLine("\nThe milk is being heated and it is being yeeted around to be nice and foamy.");
        }
    }
}
