﻿using OOPExample.Accessories;
using OOPExample.Machines;
using System;

namespace OOPExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Make a coffee machine and a soda machine. Use a milkpod.
            CoffeeMachine bigBad = new CoffeeMachine("HotnSteamy", 1999.99, "Equadorian");
            MilkPod largePod = new MilkPod(500);
            bigBad.Milk = largePod;

            SodaMachine mtnDEW = new SodaMachine("Dewy 3000", 5999, "Mountain Dew Anti Diet");
           
            // Showcase MakeDrink and Clean
            bigBad.MakeDrink();
            bigBad.Clean();
            mtnDEW.MakeDrink();
            mtnDEW.Clean();

            // Use method that takes any DrinkMachine
            MakeAndClean(bigBad);
            MakeAndClean(mtnDEW);
        }

        // Method to take any DrinksMachine and call MakeDrink and Clean
        private static void MakeAndClean(DrinksMachine machine)
        {
            machine.MakeDrink();
            machine.Clean();
        }
    }
}
