﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPExample.Machines
{
    public class CoffeeMachine : DrinksMachine
    {
        // Coffee machines have extra properties for beans, 
        // the other properties are available through inheritance
        public string BeanType { get; set; }
        public MilkPod Milk { get; set; }

        // Will call base() to let the most parent class (DrinksMachine) handle 
        // the assignment of some variables
        public CoffeeMachine() : base() { } // Default constructor is the same as Parent
        public CoffeeMachine(string Brand, double Price,
            string BeanType) : base(Brand, Price)
        {
            this.BeanType = BeanType;
        }

        // Required implementation
        public override void MakeDrink()
        {
            Console.WriteLine("\n{0}: Beans are ground up to produce the perfect brew.", Brand);
            if (Milk != null) { Milk.Heat(); }  // Using the composition to extend functionality
        }

        // Overrriden Clean method, its different for coffee machines
        public override void Clean()
        {
            Console.WriteLine("\nEmpty the grounds first, then let the machine rinse itself.");
        }
    }
}
