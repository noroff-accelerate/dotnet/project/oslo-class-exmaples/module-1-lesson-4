﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPExample.Machines
{
    public class SodaMachine : DrinksMachine
    {
        public string SodaType { get; set; }

        public SodaMachine() : base() { }

        // Milk is left out, it will chose the appropriate overloaded constructor from the parent
        public SodaMachine(string Brand, double Price, string SodaType) : base(Brand, Price)
        {
            // Once again, only need to use this.x when there is ambiguity
            this.SodaType = SodaType;
        }
        public override void MakeDrink()
        {
            Console.WriteLine("\n{0}: The sweet suagry necter poors from the heavens into your cup", Brand);
        }
        // No overriden clean, as its the same as the default.
    }
}
