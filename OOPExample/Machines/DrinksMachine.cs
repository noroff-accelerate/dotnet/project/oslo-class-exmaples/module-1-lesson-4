﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPExample.Machines
{
    public abstract class DrinksMachine
    {
        // Public properties that can be accessed by the program.
        public string Brand { get; set; }
        public double Price { get; set; }
        public string Location { get; set; }
        // Can remove set; to make properties readonly
        public bool ContainsMilk { get; }


        // Default constructor setting any default values
        public DrinksMachine()
        {
            Brand = "No name brand";
        }

        // Overloaded constructor taking in some parameters
        public DrinksMachine(string Brand, double Price)
        {
            // this.Brand refers to the Brand property of the class, Brand is the parameter passed to the constructor
            this.Brand = Brand;
            this.Price = Price;
        }

        // Abstract method that contains no implementation, must be implemented by children.
        public abstract void MakeDrink();
        // Virtual method having some default implementation that can be overriden
        public virtual void Clean()
        {
            Console.WriteLine("\nHot water is flushed through the system.");
        }
    }
}
